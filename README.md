Requisitos
=====
É preciso ter instalado na máquina:

 1. [Git (Download)](http://git-scm.com/downloads)
 2. [NodeJs+npm (Download)](http://nodejs.org/download/)
 3. [Gulp.js (Download)](https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md)
 4. [Bower (Download)](http://bower.io/)

Instalação
======
Faça o clone do projeto usando o comando:

    $ git clone git@gitlab.com:jsolemos/kit-start.git nomedosite
Depois siga até a pasta public_html e instale as dependencias do bower e npm para o gulp, usando o comando:

    $ cd nomedosite/public_html && bower install && npm install
    
Estrutura de pastas
=====

>public_html
>
>\- | **bower_components** ( Plugins: bootstrap, jquery  )
>
>\- | **node_modules** ( Especifica para modulos do node )
>
>\- | **app ( Onde fica dos os arquivos do aplicativo )**
>
>\------ | **src ( Arquivos de desenvolvimento js e css )**
>
>\-------- | css ( Salve aqui os arquivos de css  que serão unificados e minificados )
>
>\-------- | js ( Salve aqui os arquivos de js  que serão unificados e minificados )
>
>\------ | **dist ( Arquivos js e css finais unidos e minificados )**
>
>\-------- | css
>
>\------------ | allStyles.css 
>
>\------------ | allStyles.min.css 
>
>\-------- | js
>
>\------------ | allScripts.js
>
>\------------ | allScripts.min.js
>
>\- | **libs_extras** (Insira nessa pasta os arquivos css ou js de bibliotecas extras caso exista. Será gerado o arquivo extras_libs.js e extras_libs.css nas respectivas suas respectivas pastas na **dist**  com a união dos arquivos.)
>
>\- | **bower.json** ( Arquivo de instalação do **bower** )
>
>\- | **package.json** ( Arquivo de instalação do **npm** modulos do **gulp** )
>
>\- | **gulpfile.js** ( Arquivo de configuração das tarefas do **gulp** )
>
>\- | **configExtraLibs.json** ( Altere esse arquivo para configurar a ordem de concatenação dos arquivos de biblioteca extras. Informe o caminho do arquivo. )
>
>.gitignore ( Arquivo de configuração usado pelo git para ignorar o envio de pastas e/ou arquivos ao servidor. OBS: Ao Fazer o Clone e iniciar um projeto ajuste esse arquivo de acordo com suas necessidades. a pasta "public_html/node_modules" pode ser ignorada para o envio no novo projeto )

Execução
=====
Após a instalação, vá para a pasta public_html do site, execute o comando:

	$ gulp

Após isso, todo arquivo dentro da pasta **src** que for modificado será atualizado os arquivos referentes na pasta de destino a **dist**.
