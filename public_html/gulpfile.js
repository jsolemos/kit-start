//Incluindo o gulp
var gulp = require('gulp');

// Incluindo outros Plugins
var jshint = require('gulp-jshint'),
minifyCss  = require('gulp-minify-css'),
concat     = require('gulp-concat-util'),
uglify     = require('gulp-uglify'),
rename     = require('gulp-rename'),
browserSync = require('browser-sync').create(),
fs         = require('fs'),
extend = require('util')._extend,
sass = require('gulp-sass');

//Carrega arquivo de configuração de ordem de concatenação das bibliotecas extras.
//var configExtraLibs = require('./configExtraLibs.js');

var ordem_concat_libs_extras = {
    css: "libs_extras/**/*.css",
    js:"libs_extras/**/*.js"
};


var filesDir = {
        css: "./app/src/css/**/**.css",
        sass: "./app/src/sass/**/**.scss",
        js: "./app/src/js/**/**.js",
        //img: ["src/img/**/*.+(jpg|jpeg|png|gif|svg)"],
        app: "./app/**/*.+(html|php)",
        jsonExtrasLibs : 'configExtraLibs.json',
        bootstrap: './bower_components/bootstrap-sass',
    },
    distDir = {
        css:"app/dist/css/",
        sass:"app/src/css/",
        js:"app/dist/js/",
        fonts:"app/dist/fonts",
        //img: "app/dist/img/"
    };

function readJsonFileSync() {
    var file = fs.readFileSync(filesDir.jsonExtrasLibs, 'utf8');
    return file !== '' ? JSON.parse(file) : {};
}

function getJsonExtraLibs(){
    var filesList = ordem_concat_libs_extras;
    var jsonFiles = readJsonFileSync();

    if(jsonFiles.css !== undefined)
        filesList = extend(filesList,jsonFiles);

    return filesList;
}
// Static server
gulp.task('serve', function() {
    browserSync.init({
        server: {
            baseDir: "./"
        },
        ghostMode: {
            clicks: true,
            forms: true,
            scroll: true
        }
    });
});

// Tarefa para unir e minificar os arquivos css
gulp.task('css', function() {
    return gulp.src(filesDir.css)
        .pipe(concat.header('/* Arquivo: <%= file.path %>*/\n'))
        .pipe(concat.footer('\n/* Final*/\n'))
        .pipe(concat('allStyles.css'))
        .pipe(gulp.dest(distDir.css))
        .pipe(minifyCss({keepBreaks:false}))
        .pipe(rename('allStyles.min.css'))
        .pipe(gulp.dest(distDir.css))
        .pipe(browserSync.stream());
});

// Tarefa para unir e minificar os arquivos js
gulp.task('scripts', function() {
    return gulp.src(filesDir.js)
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'))
        .pipe(jshint.reporter('default'))
        .pipe(concat.header('// Arquivo: <%= file.path %>\n'))
        .pipe(concat.footer('\n// Final\n'))
        .pipe(concat('allScripts.js'))
        .pipe(gulp.dest(distDir.js))
        .pipe(rename('allScripts.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(distDir.js))
        .pipe(browserSync.stream());
});

// Tarefa para unir e minificar os arquivos js extras
gulp.task('js_libs_extras', function() {
    var filesList = getJsonExtraLibs();

    return gulp.src(filesList.js)
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'))
        .pipe(uglify({
            mangle:false
        }))
        .pipe(jshint.reporter('default'))
        .pipe(concat.header('// Arquivo: <%= file.path %>\n'))
        .pipe(concat.footer('\n// Final\n'))
        .pipe(concat('extras_libs.js'))
        .pipe(gulp.dest(distDir.js))
        .pipe(browserSync.stream());
});

// Tarefa para unir e minificar os arquivos js extras
gulp.task('css_libs_extras', function() {
    var filesList = getJsonExtraLibs();

    return gulp.src(filesList.css)
        .pipe(minifyCss({
            keepBreaks: false,
            target: './',
            root: './' //define a url absoluta para imagens
        }))
        .pipe(concat.header('/* Arquivo: <%= file.path %>*/\n'))
        .pipe(concat.footer('\n/* Final*/\n'))
        .pipe(concat('extras_libs.css'))
        .pipe(gulp.dest(distDir.css))
        .pipe(browserSync.stream());
});


gulp.task('sass', function () {
  return gulp.src('app/src/sass/app.scss')
    .pipe(rename('app.from.scss.css'))
    .pipe(sass({
        outputStyle: 'compressed',
        includePaths: [filesDir.bootstrap + '/assets/stylesheets'],
    }).on('error', sass.logError))
    .pipe(gulp.dest(distDir.sass));
});

gulp.task('fonts', function() {
    return gulp.src(filesDir.bootstrap + '/assets/fonts/**/*')
    .pipe(gulp.dest(distDir.fonts))
    .pipe(browserSync.stream());
});


// Verifica modificações nos arquivos
gulp.task('watch', function() {
    
    gulp.watch(filesDir.js, ['scripts']);
    //gulp.watch(filesDir.js_libs_extras, ['js_libs_extras']);
    //gulp.watch(filesDir.css_libs_extras, ['css_libs_extras']);
    gulp.watch(filesDir.sass, ['sass']);
    gulp.watch(filesDir.bootstrap + '/assets/fonts/**/*', ['fonts']);
    gulp.watch(filesDir.jsonExtrasLibs, ['css_libs_extras','js_libs_extras']);
    gulp.watch(filesDir.css, ['css']);
    gulp.watch(filesDir.app).on("change", browserSync.reload);

});

gulp.task('default', ['serve', 'scripts','js_libs_extras','css_libs_extras','css','watch']);
